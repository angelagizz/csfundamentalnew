const {regencies, provinces} = require('./location');

// find province with cityName //

function findProvince(regenciesName) {
    const prov = []
    let regenciesId = 0;

    for(let regency of regencies) {
        if(regency.name == regenciesName) {
            regenciesId = regency.province_id;
            break;
        }
    }
    for(let province of provinces) {
        if(province.id == regenciesId) {
            prov.push(province.name)
        }
    }
    return prov;
}
const regenciesName = "KABUPATEN CIREBON";
const prov = findProvince(regenciesName);
console.log(prov)